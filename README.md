# Ansible Cookbook
## A set of Ansible roles/playbooks to demonstrate the configuration of a set of web and application servers,
## as well as some basic infrastructure servers (a bastion host and an install of Ansible AWX).
## This particular example will use [Bookstack](https://www.bookstackapp.com/), a PHP/Laravel-based 
## wiki application.